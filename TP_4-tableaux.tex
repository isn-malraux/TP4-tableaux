\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[left=2cm,right=2cm,top=1.5cm,bottom=1.5cm]{geometry}
\usepackage{hyperref}
\usepackage{graphicx}

\usepackage{color}
\usepackage{listings}
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\ttfamily\small,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Python,                 % the language of the code
  otherkeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=4,                       % sets default tabsize to 2 spaces
  title=\lstname,                   % show the filename of files included with \lstinputlisting; also try caption instead of title
   belowskip=-1em,
}

\usepackage{fancybox}

\title{I.S.N. TP 4 -- Les tableaux}

\date{}

\newcommand\moncadre[1]
{
  \noindent\fbox{
    \begin{minipage}{\textwidth}
      #1
    \end{minipage}}
}

\begin{document}

\maketitle

\section{Introduction}

Ce TP a pour objectif d'aborder la notion de tableau en informatique. Il est basé sur les deux ressources suivantes, qu'on pourra consulter le moment venu pour avoir davantage d'explications, de vocabulaire, d'exemples.
\begin{itemize}
\item[$\bullet$] Dowek et coauteurs, Informatique et sciences du numérique, page 50 du livre, c.-à-d. page 64 du fichier. Lien \href{https://wiki.inria.fr/wikis/sciencinfolycee/images/a/a7/Informatique_et_Sciences_du_Num%C3%A9rique_-_Sp%C3%A9cialit%C3%A9_ISN_en_Terminale_S._version_Python.pdf}{ici}, 22.78 Mio
\item[$\bullet$] Apprendre à programmer avec Python 3, de Gérard Swinnen: livre disponible en format pdf (licence CC) \url{http://inforef.be/swi/download/apprendre_python3_5.pdf}, 6.12 Mio
\begin{itemize}
\item Les listes (première approche), page 44 du livre, c.-à-d. page 64 du fichier,
\item Le point sur les listes, page 141 du livre, c.-à-d. page 161 du fichier.
\end{itemize}
\end{itemize}

\section{La notion de tableau (c.-à-d. de liste en python)}

\subsection{Types simples, types composites}

En Python, nous avons déjà rencontré les types entier (\texttt{int}), nombre à virgule flottante (\texttt{float}), booléen (\texttt{bool}). On les appelle des \textit{types simples}.

Nous avons aussi rencontré le type chaîne de caractères (\texttt{str}). On dit que c'est un \textit{type composite} (une chaîne de caractère est composée de plusieurs caractères\footnote{Dans d'autres langages, il existe un autre type simple: le type caractère \texttt{char}.}).

Dans ce TP, on introduit un nouveau type composite. Ce type est appelé \textbf{tableau} dans de nombreux langages de programmation, mais en python on l'appelle \textbf{liste}.

\subsection{Définition abstraite d'une liste} 

En python, on peut considérer qu'une liste est une \textit{collection d'éléments séparée par des virgules, l'ensemble étant enfermé par des crochets}.

\subsection{Exemple}

\begin{lstlisting}[language=Python, linewidth=15cm]
# creation d'une liste, affectation a la variable t
t = [2.3, 1.6, 8.0, 5.31, 3.42, 6.7, 9.5]
type(t)
\end{lstlisting}

\noindent $\blacktriangleright\blacktriangleright$ Question A: Qu'affiche l'instruction \texttt{type(t)} ?

\subsection{Le contenu en mémoire de l'ordinateur}

En fait, l'ordinateur a réservé de la place dans la mémoire pour 7 valeurs, il a stocké ces 7 valeurs, comme schématisé par la boîte à 7 cases ci-dessous, puis il a créé une référence pour accéder au contenu de cette \og boîte \fg{}, et cette référence a été enregistrée dans la variable \texttt{t}. 

\begin{center}
\includegraphics[scale=0.65]{tableaux-1.png}
\end{center}

\subsection{Utilisation des cases d'une liste}

Les cases d'une liste sont numérotés à partir de 0. Si \texttt{t} est une liste, et si \texttt{i} est le numéro d'une case de la liste, alors la valeur contenue dans la case est \texttt{t[i]}. On peut donc:
\begin{itemize}
\item[$\bullet$] accéder à cette valeur, et par exemple l'afficher avec \texttt{print(t[i])}
\item[$\bullet$] modifier cette valeur, avec \texttt{t[i]= nouvelle\_valeur}
\end{itemize}

\begin{lstlisting}[language=Python, linewidth=15cm]
t = [2.3, 1.6, 8.0, 5.31, 3.42, 6.7, 9.5]
print(t[0])  # 2.3
print(t[2])  # 8.0
print(t[6])  # 9.5
print(t[7])  # IndexError: list index out of range
t[1]=1.0
t[5]=5.0
print(t)
\end{lstlisting}

\noindent $\blacktriangleright\blacktriangleright$ Question B: Expliquer pourquoi \texttt{print(t[7])} renvoie une erreur.\bigskip 

\noindent $\blacktriangleright\blacktriangleright$ Question C: Qu'affiche l'instruction \texttt{print(t)}  à la fin ?\bigskip

\subsection{Créer une liste de grande taille}

On peut utiliser une instruction comme par exemple \noindent\texttt{t = [0 for i in range(0,10)]} ce qui donne:

\noindent\texttt{[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]}

On peut aussi remplacer \texttt{10} par \texttt{100}, ou par une variable.

\noindent $\blacktriangleright\blacktriangleright$ Question D: Comment créer une liste dont la longueur est déterminée par l'utilisateur d'un programme ?
\vspace{1.5cm}

\subsection{Utilisation d'une boucle \texttt{pour} avec une liste}

\begin{lstlisting}[language=Python, linewidth=15cm]
lst = [0 for i in range(0,7)]
for i in range(0,7):
    lst[i] = i**2
print(lst)
\end{lstlisting}

\noindent $\blacktriangleright\blacktriangleright$ Question E: Qu'affiche l'instruction \texttt{print(lst)} ?\bigskip

\subsection{Autres instructions}

L'instruction \texttt{len(ma\_liste)} donne le nombre de cases de la liste (\texttt{len} correspond à length qui signifie longueur).
\begin{lstlisting}[language=Python, linewidth=15cm]
lettres = ['a', 'b', 'c', 'd', 'e', 'f']
print(len(lettres))
\end{lstlisting}

\noindent $\blacktriangleright\blacktriangleright$ Question F: Qu'affiche l'instruction \texttt{print(len(lettres))} ?\bigskip

\noindent $\blacktriangleright\blacktriangleright$ Question G: Pour une liste \texttt{lst}, quel est le numéro de la dernière case ? (Répondre en utilisant \texttt{len})\bigskip

\section{Exercices sur les listes}

\begin{enumerate}
\item Écrire un programme qui compte le nombre d'éléments supérieurs à 10 dans une liste d'entiers.
\item Écrire un programme qui trouve l'élément maximal dans une liste d'entiers.
\item Écrire un programme qui affiche les éléments d'une liste, du dernier au premier. Par exemple si la liste donnée est \texttt{[1, 2, 3, 4, 5, 6]}, alors le programme affiche \texttt{6 5 4 3 2 1}.
\item Écrire un programme qui remplace les éléments d'une liste par les éléments permutés de façon circulaire comme ceci: si \texttt{t=[1, 2, 3, 4, 5]} alors à la fin \texttt{t} vaudra \texttt{[2, 3, 4, 5, 1]}.
\item Écrire un programme qui analyse tous les éléments d'une liste de nombres entiers pour générer deux nouvelles listes.  L'une contiendra seulement les nombres \textit{pairs} de la liste initiale, et l'autre les nombres \textit{impairs}. Par exemple, si la liste initiale est \texttt{[32, 5, 12, 8, 3, 75, 2, 15]}, alors le programme devra construire une liste \texttt{pairs} qui contiendra \texttt{[32, 12, 8, 2]} et une liste \texttt{impairs} qui contiendra \texttt{[5, 3, 75, 15]}.
\item Exercices du site \textbf{France IOI - Découverte des tableaux}
\begin{itemize}
\item[$\bullet$] \href{http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=3}{2. Préparation de l'onguent} -- Accès à une case d'une liste
\item[$\bullet$] \href{http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=5}{4. Liste de courses} -- Accès à une case d'une liste
\item[$\bullet$] \href{http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=7}{5. Grand inventaire } -- Modifier les éléments d'un tableau
\item[$\bullet$] \href{http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=9}{6. Étude de marché} -- Tableaux de taille variable
\item[$\bullet$] \href{http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=10}{7. Répartition du poids} -- Tableaux de taille variable
\item[$\bullet$] \href{http://www.france-ioi.org/algo/task.php?idChapter=651&idTask=0&sTab=task&iOrder=11}{8. Visite de la mine} -- Tableaux de taille variable
\end{itemize}
\end{enumerate}

\section{Tableaux à deux dimensions}

On cherche à représenter de façon informatique le tableau à double entrée suivant:
\begin{tabular}{|c|c|c|c|}
\hline 
120 & 145 & 87 & 8\\ 
\hline 
12 & 67 & 89 & 24 \\ 
\hline
90 & 112 & 83 & 47 \\ 
\hline 
\end{tabular} 

\subsection{Ligne par ligne}

Une première idée est d'utiliser la liste \texttt{[120, 145, 87, 8, 12, 67, 89, 24, 90, 112, 83, 47]}. Mais dans certains cas, cette idée n'est pas pratique.

On préfère donc généralement considérer chaque ligne comme une liste, comme ceci:

\texttt{[120, 145, 87, 8]}, puis \texttt{[12, 67, 89, 24]}, puis \texttt{[90, 112, 83, 47]}.

Ensuite, on crée une \og liste de listes \fg{} comme ceci:

\texttt{t = [[120, 145, 87, 8], [12, 67, 89, 24], [90, 112, 83, 47]]}.

En Python, on dit que l'on a fait une liste à deux dimensions (et dans d'autres langages on parlera de tableaux à 2 dimensions).

On a donc, par exemple:

\noindent
\begin{minipage}{0.85\linewidth}
\begin{itemize}
\item[$\bullet$] \texttt{t[0]=[120, 145, 87, 8]}, \texttt{t[1]=[12, 67, 89, 24]}, \texttt{t[2]=[90, 112, 83, 47]}
\item[$\bullet$] \texttt{t[0][0]=120}, \texttt{t[0][1]=145}, \texttt{t[0][2]=87}, \texttt{t[0][3]=8}
\item[$\bullet$] \texttt{t[1][0]=12}, \texttt{t[1][1]=67}, \texttt{t[1][2]=89}, \texttt{t[1][3]=24}
\item[$\bullet$] \texttt{t[2][0]=90}, \texttt{t[2][1]=112}, \texttt{t[2][2]=83}, \texttt{t[1][1]=47}
\item[$\bullet$] Cas général: \texttt{t[i][j]} est le contenu de la case à la ligne \texttt{i} et à la colonne \texttt{j}.
\end{itemize}
\end{minipage}
\begin{minipage}{0.15\linewidth}
\includegraphics[scale=0.5]{tableaux-2a.png}
\end{minipage}
\medskip

On peut schématiser le contenu de la mémoire de l'ordinateur de la façon suivante:

\begin{center}
\includegraphics[scale=0.5]{tableaux-2.png}
\end{center}

La variable \texttt{t} contient une référence vers une \og boîte de 3 cases \fg{} contenant chacune une référence vers une \og boîte de 4 cases \fg{} contenant chacune un nombre.

\subsection{Colonne par colonne}

\noindent
\begin{minipage}{0.75\linewidth}
Il est tout à fait possible de faire un autre choix: considérer chaque colonne comme une liste. Dans ce cas, \texttt{t[0]=[120, 12, 90]}, \texttt{t[1]=[145, 67, 112]}, etc.

Alors \texttt{t[i][j]} est le contenu de la case à la colonne \texttt{i} et à la ligne \texttt{j}.

On utilise parfois les notations \texttt{t[x][y]} par analogie avec les abscisses et les ordonnées (pour les images informatiques, les ordonnées \og augmentent vers le bas \fg{}).
\end{minipage}
\begin{minipage}{0.15\linewidth}
\includegraphics[scale=0.6]{tableaux-2b.png}
\end{minipage}

\subsection{Générer un tableau à deux dimensions}

Exemple: on veut initialiser un tableau rempli de zéros comme ci-contre:
\begin{tabular}{|c|c|c|c|}
\hline 
0 & 0 & 0 & 0\\ 
\hline 
0 & 0 & 0 & 0\\ 
\hline 
0 & 0 & 0 & 0\\ 
\hline 
\end{tabular} 

On peut saisir les instructions suivantes:
\begin{itemize}
\item[$\bullet$] En pensant \og ligne par ligne\fg{}:
\begin{lstlisting}[language=Python]
t = [[0 for colonne in range(0, 4)] for ligne in range(0, 3)]
print(t)  # [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

# formule generale
t = [[0 for colonne in range(0, nbColonnes)] for ligne in range(0, nbLignes)]
\end{lstlisting}

\item[$\bullet$] En pensant \og colonne par colonne\fg{}:
\begin{lstlisting}[language=Python]
t = [[0 for ligne in range(0, 3)] for colonne in range(0, 4)]
print(t)  # [[0, 0, 0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]

# formule generale
t = [[0 for ligne in range(0, nbLignes)] for colonne in range(0, nbColonnes)]
\end{lstlisting}
\end{itemize}

\subsection{Exercices}

\subsubsection{Moyennes des élèves d'une classe}

Créer une liste de \texttt{3x5} éléments appelée \texttt{notes}, qui respecte les conditions suivantes:
\begin{itemize}
\item[$\bullet$] Pour \texttt{i=0..2}, \texttt{notes[i][0]} est le prénom de l'élève (une chaîne de caractères)
\item[$\bullet$] Pour \texttt{i=0..2} et \texttt{j=1..4}, \texttt{notes[i][j]} est la note numéro \texttt{j} de l'élève.
\item[$\bullet$] Notes d'Arthur: 12; 8; 15; 16. Notes de Basile: 7; 11; 14; 10. Notes de Camille: 18; 15; 6; 13.
\end{itemize}

\subsubsection{Jeu du Tic-tac-toe}

On souhaite utiliser un tableau \texttt{jeu} de dimensions \texttt{3x3} pour suivre l'avancement d'une partie de \href{https://fr.wikipedia.org/wiki/Tic-tac-toe}{Tic Tac Toe}.

Chaque case du tableau contient un caractère \texttt{'.'} ou bien \texttt{'O'} ou bien \texttt{'X'}.

Compléter le programme, composé des instructions suivantes:
\begin{itemize}
\item[$\bullet$] Créer le tableau, et l'initialiser avec des \texttt{'.'}
\item[$\bullet$] Écrire 5 instructions de type \texttt{jeu[ligne][colonne]=...} qui correspondent à la partie déjà commencée ci-dessous:
\begin{lstlisting}[linewidth=0.6cm]
..X
OOX
..O
\end{lstlisting}
\item[$\bullet$] Écrire des instructions qui permettent d'afficher le tableau de façon lisible dans la console (comme ci-dessus par exemple).
\end{itemize}
\medskip

Voici le squelette du programme à compléter:
\begin{lstlisting}[language=Python, linewidth=15cm]
# ici creation de la liste 3x3
print(jeu) # affichage pas vraiment lisible

# ici les 5 instructions de modification du tableau pour les coups joues
#
#
#
#
#

# ici les instructions pour l'affichage du jeu
#
#
#
#
\end{lstlisting}

\subsubsection{Une image}

On s'intéresse à une image de dégradé du Blanc vers le Noir, au format \texttt{pnm}, de largeur 6 pixels, et de hauteur 3 pixels. On rappelle que \texttt{BLANC=255}, et \texttt{NOIR=0}.

On décide ensuite de traiter les données sous la forme d'un tableau à deux dimensions, appelé \texttt{pixels}, de sorte que \texttt{pixels[compteurX][compteurY]} est la valeur de gris à utiliser pour le pixel de la colonne \texttt{compteurX} et de la ligne \texttt{compteurY} (en commençant à compter à partir de 0).

\begin{center}
\fbox{\includegraphics[scale=20]{degrade.png}}
\end{center}

Déterminer le contenu du tableau \texttt{pixels}, et stocker ce tableau dans une variable sous python.

\end{document}